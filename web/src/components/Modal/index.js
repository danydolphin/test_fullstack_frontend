import React from 'react'

export default function Modal({ title, children, id, dialogClass, staticModal }) {
    return <div className="modal fade" id={id} tabIndex="-1" role="dialog" aria-labelledby={id+'Label'} aria-hidden="true">
        <div className={`modal-dialog ${dialogClass}`}>
            <div className="modal-content">
                {title && 
                    <div className="modal-header">
                        <h5 className="modal-title" id={id+'Label'}>{title}</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                }
                
                <div className="modal-body">
                    {children}
                </div>
            </div>
        </div>
    </div>
}