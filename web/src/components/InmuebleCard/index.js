import React, { useContext, useState } from 'react'
import './InmuebleCard.scss'

import pixel from '../../assets/pixel.png'
import eye from '../../assets/eye-fill.svg'
import pencil from '../../assets/pencil-fill.svg'
import trash from '../../assets/trash.svg'

import GlobalContext from '../../contexts/GlobalContext'
import Swal from 'sweetalert2'
import axios from 'axios'
import { Prompt } from '../../utils/swal-mixins'

const InmuebleCard = ({inmueble, onDelete, onEdit}) => {
    const context = useContext(GlobalContext)

    const [currentImage, setCurrentImage] = useState(0)

    const handleDelete = async () => {
        try {
            const res = await Prompt.fire('Confirmación', `Quieres eliminar el inmueble con clave ${inmueble.clave}`, 'question')
    
            if (res.isConfirmed) {
                await axios.delete(`http://localhost:8000/inmuebles/${inmueble.clave}`,{
                    headers: {
                        'Authorization': `Token ${context.token}`
                    }
                })
                onDelete(inmueble)
            }

        } catch(err) {
            if (err.response) {
                console.error(err.response)
                Swal.fire('Ocurrió un error', 'inténtalo más tarde', 'error')
            }
        }
    }

    const moveCurrentImage = increment => {
        const newCurrent = currentImage + increment
        setCurrentImage(newCurrent >= inmueble.galeria.length 
            ? 0 : newCurrent < 0
            ? inmueble.galeria.length - 1 : newCurrent)
    }

    return (
        <div class="card">
            <div className="card-carousel">
                <img src={inmueble.galeria.length > 0 ? inmueble.galeria[currentImage].imagen : pixel}
                    className="cursor-pointer" alt={inmueble.nombre}
                    onClick={e => context.navigate(`/inmueble/${inmueble.clave.replace('/','_')}`)}/>
                <button className="btn card-carousel-button left"
                    onClick={e => moveCurrentImage(-1)}>&lt;</button>
                <button className="btn card-carousel-button right"
                    onClick={e => moveCurrentImage(1)}>&gt;</button>
            </div>
            <div class="card-body">
                <h5 class="card-title">{inmueble.nombre}</h5>
                <p class="card-text">{`$ ${inmueble.precio.toFixed(2)} MDP`}</p>
                <div className="d-flex align-items-start">
                    <div className="circle circle-red me-2"></div>
                    <p>{inmueble.descripcion}</p>
                </div>
                <div className="d-flex justify-content-between">
                    <img src={eye} alt="Ver información" className="icon"
                        onClick={e => context.navigate(`/inmueble/${inmueble.clave.replace('/','_')}`)}/>
                    {context.token && <>
                        <img src={pencil} alt="Editar" className="icon" onClick={e => onEdit(inmueble)}/>
                        <img src={trash} alt="Eliminar" className="icon" onClick={handleDelete}/>
                    </>
                    }
                </div>
            </div>
        </div>
    )
}

export default InmuebleCard