import React, { useEffect, useState } from 'react'
import './ImageCarousel.scss'

const ImageCarousel = ({images, alt, id, onImageClick}) => {

    const [chunkSize, setChunkSize] = useState(3)

    const calcChunkSize = () => {
        console.log(window.innerWidth)
        if (window.innerWidth > 1024)
            setChunkSize(4)
        else if (window.innerWidth > 760)
            setChunkSize(3)
        else if (window.innerWidth > 480)
            setChunkSize(2)
        else
            setChunkSize(1)
    }

    useEffect(() => {
        window.addEventListener('resize', calcChunkSize)
        calcChunkSize()

        return () => {
            window.removeEventListener('resize', calcChunkSize)
        }
    }, [])

    const chunks = []
    for (let i = 0; i < (images.length / chunkSize); i++) {
        chunks.push([])
        for (let j = 0; j < chunkSize; j++) {
            if (i * chunkSize + j >= images.length)
                break;
            chunks[i].push(images[i * chunkSize + j])
        }
    }

    console.log(chunks)

    return (
        <div id={id} class="carousel carousel-dark slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                {chunks.map((chunk, i) => (
                    <div className={'carousel-item' + (i === 0 ? ' active' : '')}>
                        <div className="row">
                            {chunk.map((image, j) => (
                                <div className={'p-4 col-'+12/chunkSize}>
                                    <img src={image} className="carousel-image d-block w-100" alt={alt}
                                        onClick={e => onImageClick(i * chunkSize + j)}/>
                                </div>
                            ))}
                        </div>
                    </div>
                ))}
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target={'#'+id} data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target={'#'+id} data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    )
}

export default ImageCarousel