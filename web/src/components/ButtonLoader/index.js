import React from 'react';

export default ({type, className, children, disabled, onClick, loading}) => (
    <button type={type || 'button'} className={className || 'btn'} disabled={disabled === undefined ? false : disabled}
        onClick={e => {onClick && onClick(e)}}>
            {loading ? 
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            :
                <span className="font-weight-bold">{children}</span>
            }
    </button>
)