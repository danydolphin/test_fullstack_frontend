import React, { useContext } from "react";
import './Navbar.scss'

import { Link } from "react-router-dom";
import GlobalContext from "../../contexts/GlobalContext";
import Swal from "sweetalert2";

const Navbar = () => {
    const context = useContext(GlobalContext)

    const logout = async () => {
        const v = await Swal.fire({
            title: 'Cerrar sesión',
            text: "Quieres cerrar sesion?",
            icon: 'question'
        })

        if (v.dismiss) return

        localStorage.removeItem('token')
        context.setToken(null)
    }

    return <>
        <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <Link className="navbar-brand" to="/">OKOL</Link>
                <ul className="navbar-nav ms-auto">
                    {context.token ?
                    <li className="nav-item">
                        <button className="btn btn-link nav-link"
                            onClick={logout}>Log out</button>
                    </li>
                    :
                    <li className="nav-item">
                        <Link className="nav-link" to="/login">Log in</Link>
                    </li>
                    }
                </ul>
            </div>
        </nav>
        <div className="navbar-clearfix"></div>
    </>
}

export default Navbar