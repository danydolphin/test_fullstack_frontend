import React, { useEffect, useRef, useState } from 'react'
import './Map.scss'

import { Wrapper } from '@googlemaps/react-wrapper'
import { createCustomEqual } from 'fast-equals'

const deepCompareEqualsForMaps = createCustomEqual(
    (deepEqual) => (a, b) => {
        if (
            a instanceof window.google.maps.LatLng ||
            b instanceof window.google.maps.LatLng
        ) {
            return new window.google.maps.LatLng(a).equals(new window.google.maps.LatLng(b));
        }

        // TODO extend to other types

        // use fast-equals for other objects
        return deepEqual(a, b);
    }
);

function useDeepCompareMemoize(value) {
    const ref = React.useRef();

    if (!deepCompareEqualsForMaps(value, ref.current)) {
        ref.current = value;
    }

    return ref.current;
}

function useDeepCompareEffectForMaps(callback, dependencies) {
    useEffect(callback, dependencies.map(useDeepCompareMemoize));
}

const Map = ({ children, ...options }) => {
    const ref = useRef(null)
    const [map, setMap] = useState()

    useEffect(() => {
        if (ref.current && !map)
            setMap(new window.google.maps.Map(ref.current, {}))
    }, [ref, map])

    useDeepCompareEffectForMaps(() => {
        if (map) {
            map.setOptions(options);
        }
    }, [map, options]);

    console.log(children)

    return (
        <>
            <div ref={ref} className='map'></div>
            {React.Children.map(children, (child) => {
                if (React.isValidElement(child)) {
                    // set the map prop on the child component
                    return React.cloneElement(child, { map });
                }
            })}
        </>
    );
}

const render = (status) => {
    return <h1>{status}</h1>
};

const MapWrapper = ({ children, zoom, center }) => (
    <Wrapper apiKey={process.env.REACT_APP_GOOGLE_API_KEY} render={render}>
        <Map zoom={zoom} center={center}>{children}</Map>
    </Wrapper>
)

export default MapWrapper