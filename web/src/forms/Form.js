import React from 'react';
import { validate } from './validate';

export default class Form extends React.Component {
  handleFormChange = e => {
    // debugger
    const name = e.target.name;
    const value = e.target.value;

    const updatedControls = {
      ...this.state.formControls,

    };
    const updatedFormElement = {
      ...updatedControls[name]
    };
    if (e.target.type === 'checkbox') {
      updatedFormElement.checked = !updatedFormElement.checked;
    } else {
      updatedFormElement.value = value;
    }
    updatedFormElement.touched = true;
    updatedFormElement.valid = validate(value, updatedFormElement.validationRules, updatedControls);
    updatedControls[name] = updatedFormElement;
    if (updatedFormElement.validationRules?.confirmationWith) {
      const confirmation = {...updatedControls[updatedFormElement.validationRules.confirmationWith]}
      confirmation.valid = validate(confirmation.value, confirmation.validationRules, updatedControls)
      updatedControls[updatedFormElement.validationRules.confirmationWith] = confirmation
    }
    let formIsValid = true;
    for (let inputIdentifier in updatedControls) {
      formIsValid = (updatedControls[inputIdentifier].valid === undefined ? false : updatedControls[inputIdentifier].valid) && formIsValid;
    }
    this.setState({
      formControls: updatedControls,
      formIsValid: formIsValid
    });
  }

  validateForm = () => {
    const fc = this.state.formControls
    for (const control in fc) {
      if (!fc[control].valid) {
        const dom = document.getElementsByName(control)[0]
        if (!dom || dom.style.display === 'none'){
          console.log('elemento invalido, pero no visible: '+control)
          continue
        }
        dom.focus()
        console.log('elemento invalido y visible: '+control)
        if (fc[control].validateSelect)
          document.getElementsByName('select_'+control)[0].click()
        this.setState({
          formControls: {...fc,
            [control]: {...fc[control],
              touched: true
            }
          }
        })
        return false
      }
    }
    return true
  }
}