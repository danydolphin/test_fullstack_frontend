import React from 'react'

import DDFile from '../../inputs/DDFile';
import Select from '../../inputs/Select';
import TextArea from '../../inputs/TextArea';
import TextInput from '../../inputs/TextInput';
import Form from '../Form';
import inmuebleFormControls from './inmuebleFormControls';
import ButtonLoader from '../../components/ButtonLoader'

import axios from 'axios'
import Swal from 'sweetalert2'
import GlobalContext from '../../contexts/GlobalContext';

export default class InmuebleForm extends Form {
    static contextType = GlobalContext

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            formIsValid: false,
            formControls: inmuebleFormControls
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.inmueble !== this.props.inmueble)
            if (this.props.inmueble)
                this.loadInmueble(this.props.inmueble)
            else
                this.resetForm()
    }

    resetForm() {
        this.setState({
            isLoading: false,
            formIsValid: false,
            formControls: inmuebleFormControls
        })
    }

    loadInmueble(inmueble) {
        console.log(inmueble)
        this.setState({
            formControls: {
                nombre: {
                    ...inmuebleFormControls.nombre,
                    valid: true,
                    value: inmueble.nombre
                },
                descripcion: {
                    ...inmuebleFormControls.descripcion,
                    valid: true,
                    value: inmueble.descripcion
                },
                clave: {
                    ...inmuebleFormControls.clave,
                    valid: true,
                    value: inmueble.clave
                },
                tipoTransaccion: {
                    ...inmuebleFormControls.tipoTransaccion,
                    valid: true,
                    value: inmueble.tipo_transaccion
                },
                tipoInmueble: {
                    ...inmuebleFormControls.tipoInmueble,
                    valid: true,
                    value: inmueble.tipo_inmueble
                },
                precio: {
                    ...inmuebleFormControls.precio,
                    valid: true,
                    value: inmueble.precio
                },
                latitud: {
                    ...inmuebleFormControls.latitud,
                    valid: true,
                    value: inmueble.lat
                },
                longitud: {
                    ...inmuebleFormControls.longitud,
                    valid: true,
                    value: inmueble.lng
                },
                galeria: {
                    ...inmuebleFormControls.galeria,
                    valid: true,
                    value: inmueble.galeria.map(i => i.imagen)
                }
            }
        })
    }

    handleSubmit = async e => {
        try {
            this.setState({isLoading: true})
            e.preventDefault()
            const {nombre, clave, descripcion, tipoTransaccion, tipoInmueble, precio, galeria, latitud, longitud} = this.state.formControls
            const data = new FormData()
            data.append('nombre', nombre.value)
            data.append('clave', clave.value)
            data.append('descripcion', descripcion.value)
            data.append('tipo_transaccion', tipoTransaccion.value)
            data.append('tipo_inmueble', tipoInmueble.value)
            data.append('precio', precio.value)
            data.append('lat', latitud.value)
            data.append('lng', longitud.value)

            for (const imagen of galeria.value)
                data.append('galeria', imagen)

            const options = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Token ${this.context.token}`
                }
            }

            const response = this.props.isEditing
                ? await axios.put(`http://localhost:8000/inmuebles/${clave.value}/`, data, options)
                : await axios.post('http://localhost:8000/inmuebles/', data, options)

            this.setState({isLoading: false})
            this.props.onSave(response.data)
        } catch(err) {
            this.setState({isLoading: false})
            console.error(err)
            if (err.response) {
                console.error(err.response)
                const message = Object.entries(err.response.data).map(e => e[1][0]).join('\n')
                Swal.fire('Ocurrió un error', message, 'error')
                return
            }
            Swal.fire('Ocurrió un error', 'Inténtalo más tarde', 'error')
        }
    }

    render() {
        const {nombre, clave, galeria, descripcion, tipoTransaccion, tipoInmueble, precio, latitud, longitud} = this.state.formControls
        const {isLoading, formIsValid} = this.state
        return <form className='row' noValidate onSubmit={this.handleSubmit}>
            <div className="col-md-6">
                <DDFile name="galeria" {...galeria} onChange={this.handleFormChange} limit={5}/>
                <h5 className="text-bold">
                    Coordenadas
                </h5>
                <TextInput name="latitud" {...latitud} onChange={this.handleFormChange}/>
                <TextInput name="longitud" {...longitud} onChange={this.handleFormChange}/>
            </div>
            <div className="col-md-6">
                <TextInput name="nombre" {...nombre} onChange={this.handleFormChange}/>
                <TextInput name="clave" {...clave} onChange={this.handleFormChange} readOnly={this.props.isEditing}/>
                <TextArea name="descripcion" {...descripcion} onChange={this.handleFormChange}/>
                <Select name="tipoInmueble" {...tipoInmueble} onChange={this.handleFormChange}/>
                <Select name="tipoTransaccion" {...tipoTransaccion} onChange={this.handleFormChange}/>
                <TextInput type="number" name="precio" {...precio} onChange={this.handleFormChange}/>
            </div>
            <div className="col-12 text-center">
                <div className="d-grid gap-2 mt-4">
                    <ButtonLoader type="submit" className="btn btn-primary btn-block" loading={isLoading} disabled={!formIsValid}>
                        Guardar
                    </ButtonLoader>
                </div>
            </div>
        </form>
    }
}