import { feedback } from '../../forms/validate';

export default {
    nombre: {
        value: '',
        label: 'Nombre del inmueble',
        placeholder: 'Hacienda Palo Alto',
        invalidFeedback: feedback.REQUIRED,
        validationRules: {
            isRequired: true
        }
    },
    clave: {
        value: '',
        label: 'Clave del inmueble',
        placeholder: 'PCOM-XXX/##',
        invalidFeedback: feedback.CLAVE_INMUEBLE,
        validationRules: {
            isClaveInmueble: true
        }
    },
    galeria: {
        value: [],
        label: 'Galería de imágenes',
    },
    descripcion: {
        value: '',
        label: 'Descripcion',
        placeholder: 'En la Hacienda Palo Alto...',
        invalidFeedback: feedback.REQUIRED,
        validationRules: {
            isRequired: true
        },
    },
    tipoTransaccion: {
        value: '',
        label: 'Tipo de transacción',
        invalidFeedback: feedback.REQUIRED,
        validationRules: {
            isRequired: true
        },
        options: [
            { text: 'Venta', value: 1 },
            { text: 'Renta', value: 2 }
        ]
    },
    tipoInmueble: {
        value: '',
        label: 'Tipo de transacción',
        invalidFeedback: feedback.REQUIRED,
        validationRules: {
            isRequired: true
        },
        options: [
            { text: 'Casa', value: 1 },
            { text: 'Departamento', value: 2 },
            { text: 'Terreno', value: 3 }
        ]
    },
    precio: {
        value: '',
        label: 'Precio',
        placeholder: '$0.00',
        invalidFeedback: feedback.PRICE,
        validationRules: {
            isDecimal: true,
            isPositive: true
        },
    },
    latitud: {
        value: '',
        label: 'Latitud',
        placeholder: '0.00000',
        invalidFeedback: feedback.DECIMAL,
        validationRules: {
            isDecimal: true,
        },
    },
    longitud: {
        value: '',
        label: 'Longitud',
        placeholder: '0.00000',
        invalidFeedback: feedback.DECIMAL,
        validationRules: {
            isDecimal: true,
        },
    }
}