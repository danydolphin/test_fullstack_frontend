export const validate = (value, rules, controls = null) => {
    let isValid = true;
    for (let rule in rules) {
        switch (rule) {
            case 'minLength': isValid = isValid && minLengthValidator(value, rules[rule]);break;
            case 'isRequired': isValid = isValid && requiredValidator(value); break;
            case 'isEmail': isValid = isValid && emailValidator(value); break;
            case 'isPhoneNumber': isValid = isValid && phoneNumberValidator(value); break;
            case 'isPassword': isValid = isValid && passwordValidator(value); break;
            case 'isUrl': isValid &= urlValidator(value); break;
            case 'isDecimal': isValid &= decimalValidator(value); break;
            case 'isPositive': isValid &= positiveValidator(value); break;
            case 'isPercentage': isValid &= percentageValidator(value); break;
            case 'confirmation': isValid = isValid && confirmationValidator(value, controls[rules[rule]].value); break;
            case 'isLower': isValid = isValid && lowerValidator(value, controls[rules[rule]].value); break;
            case 'isGreater': isValid = isValid && greaterValidator(value, controls[rules[rule]].value); break;
            case 'isClaveInmueble': isValid = isValid && claveInmuebleValidator(value); break;

            default: isValid &= true;
        }
    }
    return isValid;
}
export const feedback = {
    NUMBER: 'Ingresa un valor numérico',
    INTEGER: 'Ingresa un número entero',
    CONFIRM_PASSWORD: 'Las contraseñas no coinciden',
    PASSWORD: 'Tu contraseña debe contener al menos 8 caracteres, incluyendo al menos una mayúscula, un número y un caracter especial',
    REQUIRED: 'Este campo no puede ir vacío',
    EMAIL: 'Ingresa una dirección de correo electrónico válida',
    PHONE_NUMBER: 'Ingresa un número telefónico válido',
    URL: 'Ingresa usa URL válida',
    DECIMAL: 'Ingresa un valor entero o decimal',
    POSITIVE: 'Ingresa un valor positivo',
    PERCENTAGE: 'Ingresa un valor entre 0 y 100',
    LOWER: 'Este campo no puede ser mayor al siguiente',
    GREATER: 'Este campo no puede ser menor al primero',
    PRICE: 'Ingresa un valor monetario válido',
    CLAVE_INMUEBLE: 'La clave del inmueble debe estar en formato PCOM-XXX/##'
}

const claveInmuebleValidator = (value) => {
    return /PCOM-[A-Z]{3}\/\d{2}/.test(value)
}

const minLengthValidator = (value, minLength) => {
    return value.length >= minLength
}
const requiredValidator = value => {
    return String(value).trim() !== ''
}
export const EMAIL_PATTERN = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const emailValidator = value => {
    return EMAIL_PATTERN.test(String(value).toLowerCase())
}
const phoneNumberValidator = value => {
    return value === '' || /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(String(value))
}
const passwordValidator = value => {
    return value === '' || /(?=.*\d)(?=.*[\u0021-\u002b\u003c-\u0040])(?=.*[A-Z])(?=.*[a-z])\S{8,32}/.test(String(value));
}
const confirmationValidator = (value, confirmation) => {
    return String(value) === String(confirmation);
}
const lowerValidator = (value, confirmation) => {
    return Number(value) < Number(confirmation);
}
const greaterValidator = (value, confirmation) => {
    return Number(value) > Number(confirmation);
}
const urlValidator = value => {
    return /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/.test(String(value));
}
const decimalValidator = value => {
    return value === '' || /-?\d+(\.\d+)?/.test(value);
}
const positiveValidator = value => {
    return value === '' || /\d+(\.\d+)?/.test(value);
}
const percentageValidator = value => {
    if (!decimalValidator(value)) return false;
    const num = Number(value);
    return num >= 0 && num <= 100;
}