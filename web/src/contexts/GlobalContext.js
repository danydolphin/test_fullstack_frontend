import React from 'react'

// router context
const GlobalContext = React.createContext()

export default GlobalContext