import axios from "axios"
import React, { useContext, useEffect, useState, useLayoutEffect } from "react"
import InmuebleCard from "../components/InmuebleCard"

import Map from "../components/Map"
import Marker from "../components/Marker"
import Modal from "../components/Modal"
import Navbar from "../components/Navbar"
import GlobalContext from "../contexts/GlobalContext"
import InmuebleForm from "../forms/InmuebleForm"

import {Toast} from '../utils/swal-mixins'

const Home = () => {
    const context = useContext(GlobalContext)

    const [listaInmuebles, setListaInmuebles] = useState([])
    const [inmuebleSeleccionado, setInmuebleSeleccionado] = useState()

    useEffect(() => {
        axios.get('http://localhost:8000/inmuebles/')
            .then(res => {
                console.log(res.data)
                setListaInmuebles(res.data)
            })
            .catch(err => {
                console.log(err)
                console.error(err.response)
                Toast.fire('', 'Error cargando lista de inmuebles', 'error')
            })
    }, [])

    const openInmuebleForm = () => {
        setInmuebleSeleccionado(null)

        const modal = window.bootstrap.Modal.getOrCreateInstance(document.getElementById('modal-inmueble'))
        modal.show()
    }

    const handleInmuebleSaved = inmueble => {
        console.log(inmueble)
        if (!!inmuebleSeleccionado)
            setListaInmuebles(listaInmuebles.map(l => l.clave === inmueble.clave ? inmueble : l))
        else
            setListaInmuebles([...listaInmuebles, inmueble])

        Toast.fire('', 'Inmueble guardado con éxito', 'success')

        const modal = window.bootstrap.Modal.getOrCreateInstance(document.getElementById('modal-inmueble'))
        modal.hide()
    }

    const handleDelete = inmueble => {
        setListaInmuebles(listaInmuebles.filter(l => l.clave != inmueble.clave))
        Toast.fire('', 'Inmueble eliminado con éxito', 'success')
    }

    const handleEdit = inmueble => {
        setInmuebleSeleccionado(inmueble)

        const modal = window.bootstrap.Modal.getOrCreateInstance(document.getElementById('modal-inmueble'))
        modal.show()
    }

    return <>
        <Navbar/>
        <Map zoom={13} center={{lat: 19.4326, lng: -99.13539}}>
            {listaInmuebles.map(inmueble => (
                <Marker position={{lat: inmueble.lat, lng: inmueble.lng}}/>
            ))}
        </Map>
        <div className="container">
            <div className="row">
                {listaInmuebles.map(inmueble => (
                    <div className="col-sm-6 col-md-4">
                        <InmuebleCard inmueble={inmueble}
                            onDelete={handleDelete}
                            onEdit={handleEdit}/>
                    </div>
                ))}
            </div>
            {context.token &&
                <div className="text-end mt-4">
                    <button className="btn btn-primary" onClick={openInmuebleForm}>Agregar inmueble</button>
                </div>
            }
        </div>
        <Modal id="modal-inmueble" dialogClass="modal-lg" title="Agregar inmueble">
            <InmuebleForm inmueble={inmuebleSeleccionado}
                isEditing={!!inmuebleSeleccionado}
                onSave={handleInmuebleSaved} />
        </Modal>
    </>
}


export default Home