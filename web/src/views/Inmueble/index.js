import React, { useEffect, useState } from 'react'
import './Inmueble.scss'

import pixel from '../../assets/pixel.png'
import list from '../../assets/list-ul.svg'

import Navbar from '../../components/Navbar'
import Map from '../../components/Map'

import { useParams } from 'react-router-dom'
import axios from 'axios'
import Swal from 'sweetalert2'
import Marker from '../../components/Marker'
import ImageCarousel from '../../components/ImageCarousel'

const Inmueble = () => {
    const [inmueble, setInmueble] = useState({})

    const [currentImage, setCurrentImage] = useState(0)

    const params = useParams()

    useEffect(() => {
        axios.get('http://localhost:8000/inmuebles/' + params.clave.replace('_', '/') + '/')
            .then(res => {
                setInmueble(res.data)
                console.log(res.data)
            })
            .catch(err => {
                console.error(err.response)
                Swal.fire({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    icon: 'success',
                    title: 'Ocurrió un error intentando obtener el inmueble'
                })
            })
    }, [])

    console.log(inmueble.galeria)

    return <>
        <Navbar/>
        <div className="hero">
            <img src={inmueble.galeria?.[currentImage]?.imagen || pixel} alt={inmueble.nombre}/>
            <div className="hero-caption">
                <small>
                    <div className="circle circle-red"></div>
                    {inmueble.tipo_inmueble_display} en {inmueble.tipo_transaccion_display}
                </small>
                <h1>{inmueble.nombre}</h1>
                <h5 className='text-price'>$ {inmueble.precio?.toFixed(2)} MDP</h5>
            </div>
        </div>
        <ImageCarousel images={inmueble.galeria?.map(i => i.imagen) || []} 
            alt={inmueble.nombre}
            id="galeria-inmueble"
            onImageClick={i => setCurrentImage(i)}/>
        <div className="container">
            <h2>Detalles del inmueble</h2>
            <p>{inmueble.descripcion}</p>

            <h2 className='mt-6'>Caracteristicas del inmueble</h2>
            <div className="d-flex">
                <img src={list} alt="Caracteristicas" className="list-icon me-4"/>
                <img src={list} alt="Caracteristicas" className="list-icon me-4"/>
                <img src={list} alt="Caracteristicas" className="list-icon me-4"/>
            </div>

            <h2 className="mt-6">Ubicación</h2>

            <Map zoom={13} center={{lat: inmueble.lat, lng: inmueble.lng}}>
                <Marker position={{lat: inmueble.lat, lng: inmueble.lng}}/>
            </Map>
        </div>
    </>
}

export default Inmueble