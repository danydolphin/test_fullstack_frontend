import React from 'react'

import Navbar from '../../components/Navbar'
import ButtonLoader from '../../components/ButtonLoader';

import Form from '../../forms/Form'
import TextInput from '../../inputs/TextInput';
import { feedback } from '../../forms/validate';
import { Link } from 'react-router-dom';

import axios from 'axios'
import Swal from 'sweetalert2'
import GlobalContext from '../../contexts/GlobalContext';

export default class Login extends Form {
    static contextType = GlobalContext

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            formIsValid: false,
            formControls: {
                login_email: {
                    value: '',
                    label: 'Correo electrónico',
                    placeholder: 'correo@direcion.com',
                    valid: false,
                    touched: false,
                    invalidFeedback: feedback.EMAIL,
                    validationRules: {
                        isEmail: true,
                        isRequired: true
                    }
                },
                login_password: {
                    value: '',
                    label: 'Contraseña',
                    placeholder: '**********',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isPassword: true,
                        isRequired: true
                    },
                }
            }
        }
    }

    handleSubmit = async e => {
        try {
            e.preventDefault();
            this.setState({isLoading: true})
            const {login_email, login_password} = this.state.formControls;
            const res = await axios.post("http://localhost:8000/usuarios/login/", {
                email: login_email.value,
                password: login_password.value,
            })
            this.setState({isLoading: false})
            localStorage.setItem('token', res.data.access_token)
            this.context.setToken(res.data.access_token)
            this.context.navigate('/')
        } catch(err) {
            this.setState({isLoading: false})
            console.error(err)
            if(err.response)
                console.error(err.response)
            Swal.fire('Ocurrió un error', 'Intentalo más tarde', 'error')
        }
    }

    render() {
        const {isLoading, formIsValid} = this.state
        return <>
            <Navbar />
            <div className='container'>
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title text-center">Iniciar sesión</h5>
                                <form noValidate onSubmit={this.handleSubmit}>
                                    <TextInput 
                                        type="email" 
                                        name="login_email" 
                                        {...this.state.formControls.login_email} 
                                        onChange={this.handleFormChange}/>
                                    <TextInput type="password" name="login_password" {...this.state.formControls.login_password} onChange={this.handleFormChange}
                                        customClass="input-home" />
                                    <div className="text-center">
                                        <ButtonLoader loading={isLoading} type="submit" className="btn btn-block btn-blue mt-3" disabled={!formIsValid}>
                                            Iniciar sesión
                                        </ButtonLoader>
                                    </div>
                                </form>
                                <hr />
                                <div className="text-center">
                                    <Link to="/signup">Registrarse</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    }
}