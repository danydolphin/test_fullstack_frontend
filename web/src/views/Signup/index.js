import React from 'react'

import Navbar from '../../components/Navbar'
import ButtonLoader from '../../components/ButtonLoader';

import TextInput from '../../inputs/TextInput';
import Form from '../../forms/Form'
import signupFormControls from './signupFormControls';
import { Link } from 'react-router-dom';

import axios from 'axios'
import Swal from 'sweetalert2'
import GlobalContext from '../../contexts/GlobalContext';

export default class Login extends Form {
    static contextType = GlobalContext

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            formIsValid: false,
            formControls: signupFormControls
        }
    }

    handleSubmit = async e => {
        try {
            e.preventDefault();
            this.setState({isLoading: true})
            const {firstName, lastName, email, password} = this.state.formControls;
            const res = await axios.post("http://localhost:8000/usuarios/signup/", {
                first_name: firstName.value,
                last_name: lastName.value,
                email: email.value,
                password: password.value,
            })
            this.setState({isLoading: false})
            await Swal.fire('Correcto', 'Usuario registrado con éxito', 'success')
            localStorage.setItem('token', res.data.access_token)
            this.context.setToken(res.data.access_token)
            this.context.navigate('/')
        } catch(err) {
            this.setState({isLoading: false})
            console.error(err)
            if(err.response)
                console.error(err.response)
            Swal.fire('Ocurrió un error', 'Intentalo más tarde', 'error')
        }
    }

    render() {
        const { firstName, lastName, email, password, repassword } = this.state.formControls
        const { isLoading, formIsValid } = this.state
        return <>
            <Navbar />
            <div className='container'>
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title text-center">Iniciar sesión</h5>
                                <form noValidate onSubmit={this.handleSubmit}>
                                    <TextInput type="text" name="firstName" {...firstName} onChange={this.handleFormChange} />
                                    <TextInput type="text" name="lastName" {...lastName} onChange={this.handleFormChange} />
                                    <TextInput type="email" name="email" {...email} onChange={this.handleFormChange} />
                                    <TextInput type="password" name="password" {...password} onChange={this.handleFormChange} />
                                    <TextInput type="password" name="repassword" {...repassword} onChange={this.handleFormChange} />
                                    <div className="text-center">
                                        <ButtonLoader loading={isLoading} type="submit" className="btn btn-block btn-blue mt-3" disabled={!formIsValid}>
                                            Registrarse
                                        </ButtonLoader>
                                    </div>
                                </form>
                                <hr />
                                <div className="text-center">
                                    <Link to="/login">Ya tengo una cuenta</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    }
}