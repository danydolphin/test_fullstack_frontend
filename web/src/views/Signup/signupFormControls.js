import { feedback } from '../../forms/validate';

export default {
    firstName: {
        value: '',
        label: 'Nombre(s)',
        placeholder: 'Juan Pablo',
        invalidFeedback: feedback.REQUIRED,
        validationRules: {
            isRequired: true
        }
    },
    lastName: {
        value: '',
        label: 'Apellidos(s)',
        placeholder: 'Barajas Celis',
        invalidFeedback: feedback.REQUIRED,
        validationRules: {
            isRequired: true
        }
    },
    email: {
        value: '',
        label: 'Correo electrónico',
        placeholder: 'correo@direcion.com',
        invalidFeedback: feedback.EMAIL,
        validationRules: {
            isEmail: true,
            isRequired: true
        }
    },
    password: {
        value: '',
        label: 'Contraseña',
        placeholder: '**********',
        invalidFeedback: feedback.PASSWORD,
        validationRules: {
            isPassword: true,
            isRequired: true
        },
    },
    repassword: {
        label: 'Confirma tu contraseña',
        placeholder: '**********',
        invalidFeedback: feedback.CONFIRM_PASSWORD,
        validationRules: {
            confirmation: 'password',
            isRequired: true
        }
    }
}