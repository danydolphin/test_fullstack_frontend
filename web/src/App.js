import React, { useState } from 'react'
import './App.scss';

import {BrowserRouter as Router, Routes, Route, useNavigate} from 'react-router-dom'

import Home from './views/Home'
import Login from './views/Login';
import Signup from './views/Signup'
import Inmueble from './views/Inmueble';

import GlobalContext from './contexts/GlobalContext';

const RouterList = () => {
  const navigate = useNavigate()
  const [token, setToken] = useState(localStorage.getItem('token'))
        
  return (
    <GlobalContext.Provider value={{
      navigate,
      token, setToken
    }}>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/login" element={<Login/>}/>
        <Route path="/signup" element={<Signup/>}/>
        <Route path="/inmueble/:clave" element={<Inmueble/>}/>
      </Routes>
    </GlobalContext.Provider>
  )
}

const App = () => {
  return (
    <Router>
      <RouterList/>
    </Router>
  )
}

export default App;