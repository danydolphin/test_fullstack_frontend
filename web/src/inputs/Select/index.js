import React from 'react';

import './Select.scss';

const SelectInput = ({onToggle, name, value, selectClass, custom_select_style, options, placeholder,
        customItemsClass, showItems, invalidFeedback, valid, touched, onChange}) => {
    let validate = ''
    if (touched && !valid)
        validate = ' is-invalid'
    else if (touched)
        validate = ' is-valid'
    return <>
        <div
            className={"long-text select-selected form-control " + (selectClass || 'select-grey') + (value ? ' selected' : ' unselected') + validate}
            onClick={onToggle} name={'select_'+name}
            style={custom_select_style || {}}>
            {options?.find(a => a.value === value)?.text || placeholder || 'Selecciona un elemento'}
        </div>
        <div className={"select-items " + (customItemsClass || '') + (showItems ? " select-show" : "")}>
            {options?.map(item => {
                return (
                    <div key={item.value} className={value === item.value ? 'same-as-selected' : ''}
                        onClick={e => { onChange(item) }}>{item.text}</div>
                );
            })}
        </div>
        <div className="invalid-feedback">{invalidFeedback}</div>
    </>
}

export default class Select extends React.Component {
    state = {
        show_items: false
    }

    handleChange = item => {
        this.props.onChange({
            target: {
                name: this.props.name || 'reactive-select',
                value: item.value
            }
        });
        this.setState({ show_items: false });
        if (this.props.onShowItemsChange) this.props.onShowItemsChange(false);
    }
    toggleHiddeItems = e => {
        this.setState({ show_items: !this.state.show_items });
    }
    render() {
        const {show_items} = this.props
        return (
            <>
                {this.state.show_items &&
                    <div className="outside" onClick={e => this.setState({show_items: false})}/>
                }
                <div className="form-group reactive-select">
                    {this.props.label && <label htmlFor={this.props.name}>{this.props.label}</label>}
                    <select id={this.props.name} name={this.props.name} value={this.props.value} onChange={this.props.onChange}
                        className="select-real">
                        <option value=''>{this.props.placeholder || 'Selecciona un elemento'}</option>
                        {this.props.options?.map(item => {
                            return (
                                <option key={item.value} value={item.value}>{item.text}</option>
                            );
                        })}
                    </select>
                    <SelectInput {...this.props} onToggle={this.toggleHiddeItems}
                        showItems={(show_items === undefined && this.state.show_items) || (show_items !== undefined && show_items)}
                        onChange={this.handleChange}/>
                </div>
            </>
        )
    }
}