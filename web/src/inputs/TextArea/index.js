import React from 'react';

export default props => {
    let formControl = "form-control";
    if (props.touched && !props.valid)
        formControl = 'form-control is-invalid';
    else if (props.touched)
        formControl = 'form-control is-valid';
    return (
        <div className="form-group" style={props.style}>
            {props.label && <label htmlFor={props.name}>{props.label}</label>}
            <textarea id={props.name} className={formControl} name={props.name} placeholder={props.placeholder}
                type={props.type || 'text'} onChange={props.onChange} value={props.value || ''} readOnly={props.readOnly}
                disabled={props.disabled} autoComplete={props.autoComplete || 'on'}></textarea>
            {props.helper && props.valid &&
                <small id={props.name} class="form-text text-muted">{props.helper}</small>
            }
            <div className="invalid-feedback" style={{display: props.touched && !props.valid ? 'block' : 'none'}}>{props.invalidFeedback}</div>
        </div>
    );
};