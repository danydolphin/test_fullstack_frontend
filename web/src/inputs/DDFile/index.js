import React from 'react';

import './DDFile.scss';

import pixel from '../../assets/pixel.png';

export default class DDFile extends React.Component {
    state = {
        previews: {},
        selectedPreview: ''
    }

    componentDidMount() {
        const $ddfile = document.querySelector('#ddfile'+this.props.name);
        ['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop'].forEach(evt => {
            $ddfile.addEventListener(evt, function (e) {
                e.preventDefault()
                e.stopPropagation()
            })
        });
        ['dragover','dragenter'].forEach(evt => {
            $ddfile.addEventListener(evt, function (e) {
                $ddfile.classList.add('is-dragover')
            })
        });
        ['dragleave','dragend','drop'].forEach(evt => {
            $ddfile.addEventListener(evt, function (e) {
                $ddfile.classList.remove('is-dragover')
            })
        });
        $ddfile.addEventListener('drop', e => {
            const files = e.dataTransfer.files
            const {primaryImage, onPrimaryImageChange} = this.props
            if (!primaryImage)
                onPrimaryImageChange && setTimeout(() => onPrimaryImageChange(files[0].name))
            for (const file of files) {
                const reader = new FileReader()
                reader.onload = (ee) => {
                    this.setState({previews: {
                        ...this.state.previews,
                        [file.name]: ee.target.result
                    }})
                }        
                reader.readAsDataURL(file)
            }
            this.props.onChange({target: {
                name: this.props.name,
                value: [...(this.props.value || []), ...files]
            }})
        })
    }

    isAdvancedUpload = () => {
        const div = document.createElement('div');
        return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window;
    };

    handleInputChange = e => {
        const files = e.target.files
        const {primaryImage, onPrimaryImageChange} = this.props
        if (files.length > 0) {
            if (!primaryImage)
                onPrimaryImageChange && setTimeout(() => onPrimaryImageChange(files[0].name))
            for (let file of files) {
                const reader = new FileReader();
                reader.onload = ee => {
                    this.setState(s => ({previews: {
                        ...s.previews,
                        [file.name]: ee.target.result
                    }}));
                }
                reader.readAsDataURL(file);
            }
            this.props.onChange({target: {
                name: this.props.name,
                value: [...(this.props.value || []), ...files]
            }});
        }
    }

    cleanSelection = e => {
        this.props.onChange({
            target: {
                name: this.props.name,
                value: []
            }
        });
        this.props.onPrimaryImageChange('');
    }

    deletePreview = item => {
        const {primaryImage, value, onPrimaryImageChange} = this.props
        if (primaryImage === item || primaryImage === item.name) {
            //cambiamos a la siguiente, si hay
            let newPrimary = value[0]
            if (newPrimary === item) newPrimary = value[1]
            if (newPrimary) setTimeout(() => onPrimaryImageChange && onPrimaryImageChange(newPrimary.name || newPrimary))
        }
        if (typeof(item) === 'string')
            this.props.onChange({
                target: {
                    name: this.props.name,
                    value: this.props.value.filter(a => a !== item)
                }
            })
        else {
            const newPreviews = this.state.previews
            delete newPreviews[item.name]
            this.setState({previews: newPreviews})
            this.props.onChange({
                target: {
                    name: this.props.name,
                    value: this.props.value.filter(a => typeof(a) === 'string' || a.name !== item.name)
                }
            });
        }
    }

    render() {
        const {light, label, name, accept, value, onPrimaryImageChange, primaryImage, limit, customClass} = this.props
        const {previews} = this.state
        return <>
        <div id={"ddfile"+this.props.name} className={"ddfile" + (this.isAdvancedUpload() ? ' has-advanced-upload' : '')
            + (light ? ' ddfile-light' : ' ddfile-default') + (customClass ? ' '+customClass : '')}>
            <input className="ddfile-file" type="file" name={name} id={name}
                data-multiple-caption="{count} files selected" multiple
                onChange={this.handleInputChange} accept={accept || ''}/>
            {label && <span className="ddfile-label">{label}</span>}
            <span className="ddfile-indicator">
                {value?.length > 0 ? value.map(item => (
                    <div className={"ddfile-preview " + (typeof item == 'string' && item.startsWith('document'))} key={typeof(item) === 'string' ? item : item.name}>
                        {typeof(item) !== 'string' && !item.type.startsWith('image') ?
                            <p className="my-3 long-text" onClick={e => onPrimaryImageChange && onPrimaryImageChange(typeof(item) === 'string' ? item : item.name)}>
                                {item.name}
                            </p>
                        :
                            <img src={typeof(item) === 'string' ? item : previews[item.name] || pixel} alt='imagen'
                                className={"pointer " + (primaryImage === (typeof(item) === 'string' ? item : item.name) ? 'primary' : '')}
                                onClick={e => onPrimaryImageChange && onPrimaryImageChange(typeof(item) === 'string' ? item : item.name)}/>
                        }
                        <span className="preview-close" onClick={_ => this.deletePreview(item)}>&times;</span>
                    </div>
                )) : <span>{this.isAdvancedUpload() ? <strong>Arrastra o </strong> : ''}Sube un archivo</span>}
            </span>
            {(!limit || Number(limit) > value.length) &&
                <label htmlFor={name}>Upload</label>
            }
            <small className="pointer mt-1 font-muted" onClick={this.cleanSelection}>Limpiar selección</small>
        </div>
        </>
    }
}