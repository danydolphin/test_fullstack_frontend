# Examen Práctico: Software Engineer

Platforma de compra y venta de inmuebles.

## Frameworks y librerias utilizadas
- Front end:
    - React: 17.0.2
    - Sass: 1.45.0
    - Axios: 0.24.0
    - Sweetalert2: 11.3.0
- Back end:
    - Django: 4.0
    - djangorestframework: 3.13.0
    - mysqlclient: 2.1.0
    - Pillow: 8.4.0
    - django-cors-headers: 3.10.1

## Requerimientos
- npm >= 8.2.0
- python >= 3.0
- MariaDB >= 10.6.5

(Las versiones listadas arriba fueron las empleadas para el desarrollo del proyecto. Las versiones mínimas compatibles pueden variar.)

## Instalación

1. Instalar las dependencias de la app web:
```
cd web
npm install
```

2. Instalar las dependencias del servidor en un ambiente virtual:
```
cd back
python -m venv .venv
source .venv/bin/activate
python -m pip install -r requirements.txt
```

## Configuración

1. Cambiar la extension del archivo `web/.env.local-example` a `web/.env.local` y llenar con las variables correspondientes
2. Cambiar la extension del archivo `back/config/.env.example` a `back/config/.env` y llenar con las variables correspondientes
3. En el paso anterior se debió llenar variables con los siguiente valores:
```
DB_NAME=<DB_NAME>
DB_USER=<DB_USER>
DB_PASSWORD=<DB_PASSWORD>
```
3-1. Crear una base de datos con el nombre seleccionado
```
sudo mysql
> create database <DB_NAME>;
```
3-2. Crear un usuario con permiso sobre la base previamente creada:
```
> CREATE USER '<DB_USER>'@localhost IDENTIFIED BY '<DB_PASSWORD>';
GRANT ALL PRIVILEGES ON <DB_NAME>.* to '<DB_USER>'@localhost;
FLUSH PRIVILEGES;
```
4. Ejecutar las migraciones del servidor: 
```
cd back
source .venv/bin/activate
python manage.py migrate
```

## Ejecución

1. Arrancar el servidor de React:
```
cd web
npm start
```
2. Arrancar el servidor de Django:
```
cd back
source .venv/bin/activate
python manage.py runserver
```

Se podrá acceder al sitio web desde la url `localhost:3000`