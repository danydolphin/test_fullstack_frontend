"""Vistas para inmuebles"""

# Django REST Framework
from rest_framework import serializers, viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

# Models
from okol.inmuebles.models import Inmueble, ImagenInmueble

# Serializers
from okol.inmuebles.serializers import InmuebleModelSerializer, ImagenInmuebleSerializer

class InmueblesViewset(viewsets.ModelViewSet):
    lookup_value_regex='PCOM-[A-Z]{3}\/\d{2}'
    queryset=Inmueble.objects.all()
    serializer_class=InmuebleModelSerializer
    permission_classes=[IsAuthenticatedOrReadOnly]

    def create(self, request):
        """Registro de un inmueble. Las imagenes se guardan aparte"""
        serializer = InmuebleModelSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        inmueble = serializer.save()

        # guardamos imagenes
        for imagen in dict((request.data).lists())['galeria']:
            imagen_data = {
                'imagen': imagen,
                'inmueble': inmueble.clave
            }

            serializer = ImagenInmuebleSerializer(data=imagen_data)
            serializer.is_valid(raise_exception=True)
            imagen = serializer.save()

        return Response(InmuebleModelSerializer(Inmueble.objects.get(pk=inmueble.clave)).data)

    def update(self, request, pk=None):
        """Registro de un inmueble. Las imagenes se guardan aparte"""
        instance = self.get_object()

        serializer = InmuebleModelSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        inmueble = serializer.save()

        # eliminamos imagenes anteriores
        ImagenInmueble.objects.filter(
            inmueble=instance.clave
        ).exclude(
            imagen__in=list(filter(lambda x: isinstance(x, str) , dict((request.data).lists())['galeria']))
        ).delete()

        # guardamos imagenes
        for imagen in list(filter(lambda x: not isinstance(x, str) , dict((request.data).lists())['galeria'])):
            imagen_data = {
                'imagen': imagen,
                'inmueble': inmueble.clave
            }

            serializer = ImagenInmuebleSerializer(data=imagen_data)
            serializer.is_valid(raise_exception=True)
            imagen = serializer.save()

        return Response(InmuebleModelSerializer(Inmueble.objects.get(pk=instance.clave)).data)


