# Django
from django.apps import AppConfig

class InmueblesAppConfig(AppConfig):
    name = 'okol.inmuebles'
    verbose_name = 'Inmuebles'