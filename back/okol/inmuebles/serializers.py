"""Serializers para inmuebles"""

# Django REST Framework
from rest_framework import serializers

# Models
from okol.inmuebles.models import Inmueble, ImagenInmueble

class ImagenInmuebleSerializer(serializers.ModelSerializer):
    class Meta:
        model=ImagenInmueble
        fields='__all__'

class InmuebleModelSerializer(serializers.ModelSerializer):
    galeria=ImagenInmuebleSerializer(many=True, source='imageninmueble_set', read_only=True)
    tipo_transaccion_display = serializers.CharField(
        source='get_tipo_transaccion_display',
        read_only=True
    )
    tipo_inmueble_display = serializers.CharField(
        source='get_tipo_inmueble_display',
        read_only=True
    )
    class Meta:
        model=Inmueble
        fields='__all__'
        depth=1