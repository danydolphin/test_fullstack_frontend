"""Product models"""

# Django
from django.db import models

class Inmueble(models.Model):
    """ Modelo de productos """

    # Clave de producto, en formato PCOM-XXX/##
    clave = models.CharField(
        primary_key=True,
        max_length=11
    )

    # Nombre del producto
    nombre = models.CharField(max_length=100)

    # Descripcion del producto
    descripcion = models.CharField(max_length=1500)

    # Transaccion 1: venta
    VENTA = 1
    # Transaccion 2: renta
    RENTA = 2
    TIPO_TRANSACCION_OPCIONES = [
        (VENTA, 'venta'),
        (RENTA, 'renta'),
    ]
    # Tipo de transaccion
    tipo_transaccion = models.IntegerField(choices=TIPO_TRANSACCION_OPCIONES)

    # Transaccion 1: venta
    CASA = 1
    # Transaccion 2: renta
    DEPARTAMENTO = 2
    # Inmueble 3: terreno
    TERRENO = 3
    TIPO_INMUEBLE_OPCIONES = [
        (CASA, 'casa'),
        (DEPARTAMENTO, 'departamento'),
        (TERRENO, 'terreno'),
    ]
    # Tipo de inmueble
    tipo_inmueble = models.IntegerField(choices=TIPO_INMUEBLE_OPCIONES)
    
    # Precio del inmueble
    precio = models.FloatField()

    # Latitud
    lat = models.FloatField(default=0)

    # Longitud
    lng = models.FloatField(default=0)

    class Meta:
        db_table = 'inmueble'
        ordering = ['clave']

    def __str__(self):
        return self.clave

class ImagenInmueble(models.Model):
    """ Modelo de imagenes de productos """
    imagen = models.ImageField(upload_to='inmuebles/')
    inmueble = models.ForeignKey(Inmueble, on_delete=models.CASCADE)
    class Meta:
        db_table='imageninmueble'
        