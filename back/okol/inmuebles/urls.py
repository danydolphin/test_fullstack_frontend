"""URLs para inmuebles"""

# Django
from django.urls import include, path

# Django REST Framework
from rest_framework.routers import DefaultRouter

# Vistas
from .views import InmueblesViewset

router = DefaultRouter()
router.register(r'inmuebles', InmueblesViewset, basename='inmuebles')

urlpatterns = [
    path('', include(router.urls))
]