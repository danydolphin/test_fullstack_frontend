"""Users serializers."""

# Django
from django.contrib.auth import password_validation, authenticate

# Django REST Framework
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.validators import UniqueValidator

# Models
from okol.usuarios.models import Usuario

class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model=Usuario
        fields = (
            'email',
            'first_name',
            'last_name',
        )

class SignUpSerializer(serializers.Serializer):
    """Serializer para signup
    Valida los datos y crea al usuario
    """

    # email
    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=Usuario.objects.all())]
    )

    # Password
    password = serializers.CharField(min_length=8, validators=[password_validation.validate_password])

    # Name
    first_name = serializers.CharField(min_length=2, max_length=30)
    last_name = serializers.CharField(min_length=2, max_length=30)

    def create(self, data):
        """Crea al usuario."""
        user = Usuario.objects.create_user(**data)
        return user


class LoginSerializer(serializers.Serializer):
    """Serializer para login"""

    email = serializers.EmailField()
    password = serializers.CharField(min_length=8)

    def validate(self, data):
        """Checa las credenciales"""
        user = authenticate(username=data['email'], password=data['password'])
        if not user:
            raise serializers.ValidationError('Usuario o contraseña incorrectos')
        self.context['user'] = user
        return data

    def create(self, data):
        """Crea o regresa el token del usuario"""
        token, created = Token.objects.get_or_create(user=self.context['user'])
        return self.context['user'], token.key