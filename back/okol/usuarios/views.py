"""Vistas de usuario"""

# Django REST Framework
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.response import Response

# Usuarios
from okol.usuarios.models import Usuario

# Permisos
from rest_framework.permissions import AllowAny, IsAuthenticated
from okol.usuarios.permissions import IsAccountOwner

# Serializers
from okol.usuarios.serializers import (
    LoginSerializer, 
    SignUpSerializer, 
    UsuarioSerializer
)

class UsuariosViewSet(mixins.UpdateModelMixin,
                      viewsets.GenericViewSet):
    """Vistas de usuario
    Contiene vistas para signup, login"""

    queryset = Usuario.objects.filter(is_staff=False)
    serializer_class = UsuarioSerializer
    lookup_field = 'email'

    def get_permissions(self):
        """Asigna permisos basado en las acciones."""
        if self.action in ['signup', 'login']:
            permissions = [AllowAny]
        elif self.action in ['retrieve', 'update', 'partial_update', 'profile']:
            permissions = [IsAuthenticated, IsAccountOwner]
        else:
            permissions = [IsAuthenticated]
        return [p() for p in permissions]

    @action(detail=False, methods=['post'])
    def login(self, request):
        """Login"""
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = serializer.save()
        data = {
            'user': UsuarioSerializer(user).data,
            'access_token': token
        }
        return Response(data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['post'])
    def signup(self, request):
        """User sign up."""
        serializer = SignUpSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        data = UsuarioSerializer(user).data
        return Response(data, status=status.HTTP_201_CREATED)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_user(request):
    return Response(UsuarioSerializer(request.user).data)