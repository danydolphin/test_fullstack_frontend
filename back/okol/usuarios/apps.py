# Django
from django.apps import AppConfig

class UsuariosAppConfig(AppConfig):
    name = 'okol.usuarios'
    verbose_name = 'Usuarios'