"""User's models"""

# Django
from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

class UsuarioManager(BaseUserManager):
    """Manager para usuarios
    
    Necesario para sobreescribir los métodos create_user y create_superuser,
    ya que estos esperan un username en lugar de email como identificador
    """

    def create_user(self, email, password, **extra_fields):
        """
        Crea y guarda un usuario con el email y la contraseña dados
        """
        if not email:
            raise ValueError(_('Debes ingresar un correo electrónico'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

class Usuario(AbstractUser):
    """Modelo de Usuario.
    Extiende de la clase de Django AbstractUser, cambia el campo de username
    por el email y agrega información extra
    """

    email = models.EmailField(
        'Dirección de email',
        unique=True,
        error_messages={
            'unique': 'Ya existe un usuario con este email.'
        }
    )

    """ Eliminamos el campo de username: no lo necesitamos """
    username = None

    """Llave primaria = email"""
    USERNAME_FIELD = 'email'

    """Campos requeridos para crear un usuario"""
    REQUIRED_FIELDS = ['first_name', 'last_name']

    # Registramos el nuevo manager
    objects = UsuarioManager()

    class Meta:
        db_table="usuarios"

    def __str__(self):
        """Return username."""
        return self.email

    def get_short_name(self):
        """Return username."""
        return self.email

