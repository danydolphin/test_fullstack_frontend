"""Permisos para vistas de usuario"""

# Django REST Framework
from rest_framework.permissions import BasePermission

class IsAccountOwner(BasePermission):
    """Otorga permiso al dueño del usuario."""

    def has_object_permission(self, request, view, obj):
        """Check obj and user are the same."""
        return request.user == obj