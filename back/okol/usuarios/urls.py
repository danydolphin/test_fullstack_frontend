"""URLs para usuarios."""

# Django
from django.urls import include, path

# Django REST Framework
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

# Vistas
from .views import UsuariosViewSet, get_user

router = DefaultRouter()
router.register(r'usuarios', UsuariosViewSet, basename='usuarios')

urlpatterns = [
    path('', include(router.urls))
] + format_suffix_patterns([
    path('usuarios/', get_user)
])